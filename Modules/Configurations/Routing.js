module.exports = {
    Initiate: function (AppRequiresObj) {
        AppRequiresObj.ExpressObj.get("/getperson", AppRequiresObj.validateUser, function(requestVariable, responseVariable){
            AppRequiresObj.ReadControllerObj.GetPersonById(AppRequiresObj.MongoDBObj, responseVariable, GetParsedQueryString(AppRequiresObj, requestVariable));
        });
        AppRequiresObj.ExpressObj.delete("/deleteperson", AppRequiresObj.validateUser, function(requestVariable, responseVariable){
            AppRequiresObj.DeleteControllerObj.DeletePersonById(AppRequiresObj.MongoDBObj, responseVariable, GetParsedQueryString(AppRequiresObj, requestVariable));
        });
        AppRequiresObj.ExpressObj.post("/insertperson", AppRequiresObj.validateUser, function(requestVariable, responseVariable){
            AppRequiresObj.CreateControllerObj.CreatePerson(AppRequiresObj.MongoDBObj, responseVariable, GetParsedQueryString(AppRequiresObj, requestVariable));
        });
        AppRequiresObj.ExpressObj.post("/updateperson", AppRequiresObj.validateUser, function(requestVariable, responseVariable){
            AppRequiresObj.UpdateControllerObj.UpdatePersonById(AppRequiresObj.MongoDBObj, responseVariable, GetParsedQueryString(AppRequiresObj, requestVariable));
        });
        AppRequiresObj.ExpressObj.post("/registration", function(requestVariable, responseVariable){
          AppRequiresObj.RegistrationControllerobj.UserRegistration(AppRequiresObj.MongoDBObj, responseVariable, GetParsedQueryString(AppRequiresObj, requestVariable));  
        });
        AppRequiresObj.ExpressObj.get("/getalluser", function(requestVariable, responseVariable){
          AppRequiresObj.RegistrationControllerobj.GetAllUsers(AppRequiresObj.MongoDBObj, responseVariable);  
        });
        AppRequiresObj.ExpressObj.post("/userlogin", function(requestVariable, responseVariable){
            AppRequiresObj.UserloginControllerobj.Userlogin(AppRequiresObj, requestVariable, responseVariable, GetParsedQueryString(AppRequiresObj, requestVariable));
        });
        
        AppRequiresObj.ExpressObj.get("/", function(requestVariable, responseVariable) {
            responseVariable.write("error");
            responseVariable.end();
        });
        AppRequiresObj.ExpressObj.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type," + 
            "Accept, x-client-key, x-client-token, x-client-secret, Authorization");
            next();
          });
    }
}
function GetParsedQueryString (AppRequiresObj, requestVariable){
    let UrlPath = requestVariable.url;
    return AppRequiresObj.url.parse(requestVariable.url, true).query; //Split the url (query string) into readable parts
}