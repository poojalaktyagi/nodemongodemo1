const { maxHeaderSize } = require("http");

module.exports = {
    CreatePerson:  async function(requestVariable, responseVariable, queryStringObj){
        if(queryStringObj !== undefined && queryStringObj.firstname !== undefined){
            let AllPersonData = await MongoDBObj.collection("PersonMaster").find({}).toArray();
            let MaxPersonId = 0;
            for(let i = 0; i < AllPersonData.length; i++)
            {
                if(MaxPersonId < AllPersonData[i].PersonId)
                    MaxPersonId = AllPersonData[i].PersonId;
            }
            MaxPersonId++;
            let LastName = null,
                Age = null,
                BirthPlace = null;
            if(queryStringObj.lastname !== undefined){
                LastName = queryStringObj.lastname;
            }
            if(queryStringObj.age !== undefined){
                Age = queryStringObj.age;
            }
            if(queryStringObj.birthplace !== undefined){
                 BirthPlace = queryStringObj.birthplace;
            }                

            MongoDBObj.collection("PersonMaster")
                        .insertOne({PersonId: MaxPersonId, FirstName: queryStringObj.firstname, LastName: LastName,
                                        Age: Age, BirthPlace: BirthPlace
                        }, function(err, result) {
                if (err) throw err;

                //console.log(result);
                responseVariable.writeHead(200, {'Content-Type': 'text/json'});
                responseVariable.write(JSON.stringify(result));
                responseVariable.end();
              });
        }
        else{
            responseVariable.write("error: No query string/firstname");
            responseVariable.end();
        }
    }
}    