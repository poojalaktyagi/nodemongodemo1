module.exports = {
  ConnectToDB : function(){
    var MongoClientVariable = require('mongodb').MongoClient;
    var MongoServiceUrl = "mongodb://localhost:27017/";

    MongoClientVariable.connect(MongoServiceUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
      if (err) throw err;

      global.MongoDBObj = db.db("NodeMongoDemo1"); // MongoDBObj is just name
    });
  }
}