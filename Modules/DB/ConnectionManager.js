module.exports = function (AppRequiresObj) {
   AppRequiresObj.MongoClientObj.connect(AppRequiresObj.Config.MongoServiceUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
    if (err) throw err;
    AppRequiresObj.MongoDBObj = db.db("NodeMongoDemo1"); // MongoDBObj is just name
  });
}