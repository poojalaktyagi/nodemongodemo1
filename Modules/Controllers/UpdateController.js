const { maxHeaderSize } = require("http");

module.exports = {
    UpdatePersonById: async function (MongoDBObj, responseVariable, queryStringObj) {
        if (queryStringObj !== undefined && queryStringObj.personid !== undefined) {
            let personid = parseInt(queryStringObj.personid);
            if (!isNaN(personid) && personid > 0) {
                let LastName = null,
                    Age = null,
                    BirthPlace = null;
                if (queryStringObj.lastname !== undefined) {
                    LastName = queryStringObj.lastname;
                }
                if (queryStringObj.age !== undefined) {
                    Age = queryStringObj.age;
                }
                if (queryStringObj.birthplace !== undefined) {
                    BirthPlace = queryStringObj.birthplace;
                }

                MongoDBObj.collection("PersonMaster")
                    .updateOne({PersonId: personid},
                        {$set: {FirstName: queryStringObj.firstname, LastName: LastName,
                        Age: Age, BirthPlace: BirthPlace
                    }}, function (err, result) {
                        if (err) throw err;

                        //console.log(result);
                        responseVariable.writeHead(200, { 'Content-Type': 'text/json' });
                        responseVariable.write(JSON.stringify(result));
                        responseVariable.end();
                    });
            }
            else {
                responseVariable.write("error: No query string/personid");
                responseVariable.end();
            }
        }
        else {
            responseVariable.write("error: No query string/personid");
            responseVariable.end();
        }
    }
}    