module.exports = {
    UserRegistration: async function (MongoDBObj, responseVariable, queryStringObj) {
        if (queryStringObj != undefined && queryStringObj.firstname != undefined && queryStringObj.lastname != undefined && queryStringObj.username != undefined && queryStringObj.password != undefined) {
            let AllLoginData = await MongoDBObj.collection("LoginMaster").find({}).toArray();
            let MaxLoginId = 0;
            for (let i = 0; i < AllLoginData.length; i++) {
                if (MaxLoginId < AllLoginData[i].loginId)
                    MaxLoginId = AllLoginData[i].loginId;
            }
            MaxLoginId++;
            MongoDBObj.collection("LoginMaster")
                .insertOne({
                    loginId: MaxLoginId, FirstName: queryStringObj.firstname, LastName: queryStringObj.lastname,
                    UserName: queryStringObj.username, password: queryStringObj.password
                },
                    function (err, result) {
                        if (err) throw err;
                        
                        //console.log(result);
                        responseVariable.writeHead(200, { 'Content-Type': 'text/json' });
                        responseVariable.write(JSON.stringify(result));
                        responseVariable.end();
                    });
        }
        else {
            responseVariable.write("error: No query string/firstname/lastname/username/password");
            responseVariable.end();
        }
    },
    GetAllUsers: async function(MongoDBObj, responseVariable){
        MongoDBObj.collection("LoginMaster")
            .find().toArray(function(err, result) {
                if (err) throw err;

                //console.log(result);
                responseVariable.writeHead(200, {'Content-Type': 'text/json'});
                responseVariable.write(JSON.stringify(result));
                responseVariable.end();
              });

    }

}