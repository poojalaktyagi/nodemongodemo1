module.exports = {
    GetPersonById: function (MongoDBObj, responseVariable, queryStringObj) {
        if (queryStringObj !== undefined) {
            let personid = parseInt(queryStringObj.personid);
            if (!isNaN(personid) && personid > 0) {
                MongoDBObj.collection("PersonMaster").find({ PersonId: personid }).toArray(function (err, result) {
                    if (err) throw err;

                    //console.log(result);
                    responseVariable.writeHead(200, { 'Content-Type': 'text/json' });
                    responseVariable.write(JSON.stringify(result));
                    responseVariable.end();
                });
            }
            else {
                MongoDBObj.collection("PersonMaster").find().toArray(function (err, result) {
                    if (err) throw err;

                    //console.log(result);
                    responseVariable.writeHead(200, { 'Content-Type': 'text/json' });
                    responseVariable.write(JSON.stringify(result));
                    responseVariable.end();
                });
            }
        }
        else {
            responseVariable.write("error");
            responseVariable.end();
        }
    }
}