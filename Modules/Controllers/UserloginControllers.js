const { query } = require("express");

module.exports = {
    Userlogin: function (AppRequiresObj, requestVariable, responseVariable, queryStringObj) {
        //console.log(MongoDBObj);
        if (queryStringObj != undefined && queryStringObj.username != undefined && queryStringObj.password != undefined) {
            AppRequiresObj.MongoDBObj.collection("LoginMaster").findOne({ UserName: queryStringObj.username, password: queryStringObj.password }, function (err, result) {
                if (err) throw err;
                //console.log(result);
                let ResponseJSON = {};
                if (result == null) {
                    ResponseJSON = { "Authenticated": false };
                }
                else {
                    const token = AppRequiresObj.jwt.sign({ id: result.loginId }, requestVariable.app.get('secretKey'), { expiresIn: '1h' });
                    responseVariable.cookie("jwtoken", token);
                    ResponseJSON = { status: "success", message: "user found!!!", data: { user: result, token: token } };
                }
                responseVariable.json(ResponseJSON);
                responseVariable.end();

            });
        }
    }
}