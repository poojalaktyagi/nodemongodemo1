module.exports = {
    DeletePersonById: function(MongoDBObj, responseVariable, queryStringObj){
        console.log("here", queryStringObj);
        if(queryStringObj !== undefined){
            let personid = parseInt(queryStringObj.personid);
            if(!isNaN(personid) && personid > 0){
                MongoDBObj.collection("PersonMaster").deleteOne({PersonId: personid}, function(err, result) {
                    if (err) throw err;
                    //console.log(result);
                    responseVariable.writeHead(200, {'Content-Type': 'text/json'});
                    responseVariable.write(JSON.stringify(result));
                    responseVariable.end();
                  });
            }
            else{
                responseVariable.write("error");
                responseVariable.end();
            }
        }
        else{
            responseVariable.write("error");
            responseVariable.end();
        }
    }
}