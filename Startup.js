function AppRequires() {
  this.Config = require("./Modules/Configurations/config.json");

  let express = require('express');
  this.http = require('http');
  this.jwt = require('jsonwebtoken');
  this.url = require('url');
  let MongoDB = require('mongodb');
  const bodyParser = require("body-parser");
  const cookieParser = require("cookie-parser");

  this.ExpressObj = express();
  const cors = require('cors'); 
  this.ExpressObj.use(cors());
  this.ExpressObj.set('secretKey', 'pooja_sceret');
  this.ExpressObj.use(bodyParser.urlencoded({ extended: false }));
  this.ExpressObj.use(cookieParser());

  this.MongoClientObj = MongoDB.MongoClient;

  this.DBConnObj = require('./Modules/DB/ConnectionManager.js');
  this.ReadControllerObj = require("./Modules/Controllers/ReadController.js");
  this.DeleteControllerObj = require("./Modules/Controllers/DeleteController.js");
  this.CreateControllerObj = require("./Modules/Controllers/CreateController.js");
  this.UpdateControllerObj = require("./Modules/Controllers/UpdateController.js");
  this.RegistrationControllerobj = require("./Modules/Controllers/RegistrationControllers.js");
  this.UserloginControllerobj = require("./Modules/Controllers/UserloginControllers.js");
  this.RoutingObj = require('./Modules/Configurations/Routing.js');

  let selfAppRequires = this;
  this.validateUser = function (req, res, next) {
    let jwtoken = req.cookies["jwtoken"];
    //console.log("hello: " + jwtoken);
    selfAppRequires.jwt.verify(jwtoken, req.app.get('secretKey'), function (err, decoded) {
      if (err) {
        res.json({ status: "error", message: err.message, data: null });
      } else {
        // add user id to request
        req.body.userId = decoded.id;
        next();
      }
    });
  }
}

let AppRequiresObj = new AppRequires();
AppRequiresObj.DBConnObj(AppRequiresObj);
AppRequiresObj.RoutingObj.Initiate(AppRequiresObj);
AppRequiresObj.http.createServer(AppRequiresObj.ExpressObj).listen(8080, function () {
  console.log('Started!');
});